function drawLegend(svg, color, users) {
	var legend, legendRectSize, legendSpacing;
	
	legendRectSize = 18;
	legendSpacing = 4;

	legend = svg.selectAll('.legend')
		.data(color.domain())
		.enter()
		.append('g')
		.attr('class', 'legend')
		.style("fill", "#2f2a20")
		.attr('transform', function(d, i) {
			var height = legendRectSize + legendSpacing;
			var offset =  height * color.domain().length / 2;
			var horz = legendRectSize;
			var vert = 220 + (i * (height*-1));
			return 'translate(' + horz + ',' + vert + ')';
		});
	legend.append('rect')
		.attr('width', legendRectSize)
		.attr('height', legendRectSize)
		.style('fill', color)
		.style('stroke', color);

	legend.append('text')
		.attr('x', legendRectSize + legendSpacing)
		.attr('y', legendRectSize - legendSpacing)
		.text(function(d) {
			return users[d];
		});
}

function drawPercentageLabel(svg, axis, text, color) {
	// the percentage label
  svg.append("g")
      .attr("class", "axis")
      .call(axis)
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", -50)
      .attr("x", -20)
      .attr('class','label')
      .attr("dy", "1em")
      .style("text-anchor", "end")
      .style("fill", color)
      .text(text);
}

function drawLine(svg, count, users, user) {
		svg.append("path")
			.datum(users[user])
			.attr("class", "line line-" + user)
			.attr("d", line)
			.attr("fill-opacity", 0)
			.attr("stroke-width", 3)
			.attr("stroke", function(d, i) {
				return color(count);
			})
			.style("stroke-linecap", "round")
			.style("stroke-linejoin", "round");
		
}

function drawLines(svg, users) {
	count = 0;
	for(user in users) {
		drawLine(svg, count, users, user);
		count++;
	}
}


