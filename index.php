<?php
/**
 * Mouse Life
 * @author John Pennypacker <john_pennypacker@brown.edu>
 */

require_once 'inc/functions.php';

if(config_exists()) {
	include_once 'config.php';
} else {
	die('<p>You must create a config.php to use this application.</p>');
}


$users = array('jpennypa', 'wdennen', 'joconno1');

foreach($users as $u) {
	$$u = parse_log($u . '-mouse-battery-status.txt');
}

?>
<!doctype html>
<head>
	<title>Mouse Battery Life</title>
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,600|Ultra" rel="stylesheet" type="text/css">
	<link href="<?php print $base_path; ?>/css/mouse.css" rel="stylesheet" type="text/css">
	<script src="http://d3js.org/d3.v3.min.js"></script>
	<script src="<?php print $base_path; ?>/js/min/mouse.min.js"></script>
</head>
<body>
	<div class="container">
		<h1>Mouse Battery Life</h1>
	</div>
</body>

<script>
	
	var w, h, color, margin, width, height, parseDate, x, y, xAxis, yAxis, line, svg, allUsersData, usersList, users, <?php print implode(', ', $users); ?>;
	w = 30;
	h = 300;
	
	color = d3.scale.ordinal()
		.range(['#50853b', '#bd5b34', '#4f9895', '#264079', '#c19a14', '#8f856d', '#c7b790']); 

	margin = {top: 20, right: 20, bottom: 30, left: 50},
		width = 960 - margin.left - margin.right,
		height = 300 - margin.top - margin.bottom;

	parseDate = d3.time.format("%Y-%m-%dT%H:%M:%S").parse;

	x = d3.time.scale()
		.range([0, width])

	y = d3.scale.linear()
		.range([height, 0]);

	xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom");

	yAxis = d3.svg.axis()
		.scale(y)
		.orient("left");

	line = d3.svg.line()
		.x(function(d) { return x(d.date); })
		.y(function(d) { return y(d.percentage); });

	svg = d3.select(".container").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");


	usersList = ["<?php print implode('", "', $users); ?>"];
	users = {};

	<?php
		foreach($users as $u) {
			$name = $u . '_array';
			$$name = array();
			foreach($$u as $v) {
				array_push($$name, '["' . $v['time'] . '",' . $v['percentage'] . ']');
			}
			print $u . ' = [' . implode(', ', $$name) . ']' . "\r\n";
			print "\tusers.{$u} = $u.map(function(d) { return { date: parseDate(d[0]), percentage: d[1] }; });" . "\r\n";
		}
	?>



	allUsersData = [].concat(users.wdennen, users.jpennypa, users.joconno1);

	d3.selectAll("li").style("display", "none");

	x.domain(d3.extent(allUsersData, function(d) { return d.date; }));
	y.domain(d3.extent(allUsersData, function(d) { return d.percentage; }));

	svg.append("g")
      .attr("class", "axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

	drawLines(svg, users);
	drawPercentageLabel(svg, yAxis, "Percentage", "#2f2a20");
	drawLegend(svg, color, usersList);


</script>

</html>