<?php
/**
 * Configuration options to match local environment
 */

// the URL path relative from the host root
$base_path = ''; //no trailing slash

$app_path = set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__));

// database credentials
define('DB_NAME', 'mouse');
define('DB_USER', 'mouse');
define('DB_PASSWORD', 'password');
define('DB_HOST', 'localhost');