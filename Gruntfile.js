'use strict';

module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),

		tag: {
			banner: '/*!\n' +
							' * <%= pkg.name %>\n' +
							' * <%= pkg.title %>\n' +
							' * <%= pkg.url %>\n' +
							' * <%= grunt.template.today("yyyy-mm-dd") %>\n' +
							' * @author <%= pkg.author %>\n' +
							' * @version <%= pkg.version %>\n' +
							' * Copyright <%= pkg.copyright %>. <%= pkg.license %> licensed.\n' +
							' */\n'
		},

		concat: {
			dist: {
				src: [
					'scss/*.scss',
				],
				dest: 'scss/build.scss',
			 }
		},
				 
		sass: {
			prod: {
				options: {
					style: 'compressed',
					compass: true
				},
				files: {
					'css/<%= pkg.name %>.css': 'scss/build.scss'
				}
			}
		},

		uglify: {
			options: {
				//banner: '<%= tag.banner %>'
			},
			build: {
				src: 'js/<%= pkg.name %>.js',
				dest: 'js/min/<%= pkg.name %>.min.js'
			}
		},

		// watch the haml, js, and css files for changes
		watch: {
			javascript: {
				files: 'js/*.js',
				tasks: ['uglify']
			},
			sass: {
				cwd: 'scss/',
				files: '**/*.scss',
				tasks: ['concat', 'sass']
			}
		}

	});

	// Load Grunt plugins
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	// Default task(s).
	grunt.registerTask('default', ['uglify', 'concat', 'sass', 'watch']);

};