<?php
/**
 * Mouse Life Listener
 * test curl:
 * curl --data "user=jpennypa&time=&percent=81" http://ppkr.local/mouse/listener.php; echo
 * @author John Pennypacker <john_pennypacker@brown.edu>
 */

require_once 'inc/functions.php';
require_once 'api/v1/mouse.php';

if(config_exists()) {
	include_once 'config.php';
} else {
	die('<p>You must create a config.php to use this application.</p>');
}

if(isset($_POST['user']) && isset($_POST['percent'])) {
	$input = array(
		 'user' => $_POST['user']
		,'time' => $_POST['time']
		,'percent' => $_POST['percent']
	);
} else {
	var_dump($_POST);
	die('not enough data to record the status');
}


$uid = is_valid_user($input['user']);

if($uid !== FALSE && is_valid_percentage($input['percent'])) {
	$time = (is_valid_time($input['time'])) ? $input['time'] : '';
	$success = create_mouse_record($uid, $input['percent'], $time);
	var_dump($success);
} else {
	die('input does not meet minimum requirements');
}

?>