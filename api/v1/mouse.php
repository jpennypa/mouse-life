<?php
/**
 * Mouse Life PHP API
 * @author John Pennypacker <john_pennypacker@brown.edu>
 */

require_once 'config.php';
require_once 'inc/functions.php';

/**
 * Create a new row in the database
 * @param int $uid is the user id
 * @param int $percentage the percentage of battery charge
 * @param str $time the created_at value
 * @return bool
 */
function create_mouse_record($uid, $percentage, $time='') {
	if(empty($time)) {
		$time = date('Y-m-d H:i:s', time());
	} else {
		$time = date('Y-m-d H:i:s', strtotime($time));
	}
	$pdo = db_connect();
	$sql = 'INSERT INTO status
					( uid,  created_at,  percentage)
					VALUES
					(:uid, :created_at, :percentage)
	';
		
	$query = $pdo->prepare($sql);
	$success = $query->execute(array(':uid' => $uid, ':created_at' => $time, ':percentage' => $percentage));
	if(!$success) {
		print_r($query->errorInfo());
	}
	return $success;
}

/**
 * Read a single percentage row
 * @param int $sid the id of the row
 * @return arr
 */
function read_mouse_record($sid) {
	$pdo = db_connect();
	$sql = 'SELECT s.*, u.name name 
					FROM status
					LEFT JOIN users ON s.uid = u.id
					WHERE 
						s.id = :sid
	';
	$query = $pdo->prepare($sql);
	$success = $query->execute(array(':sid' => $sid));

	if($success) {
		$result = $query->fetchAll();
	} else { // db error
		$result = FALSE;
		print_r($query->errorInfo());
	}
	
	return $result;
}

/**
 * Read a range of percentage rows
 * @param str $start is the start date
 * @param str $end is the end date
 * @param arr $users an array of user ids
 * @return arr
 */
function read_mouse_records($start, $end, $users) {
	$pdo = db_connect();
	$where = array();

	// check the params
	if(!empty($start)) {
		$start = strtotime($start);
		$where[] = ' s.created_at > ' . $start . ' ';
	}
	if(!empty($end)) {
		$end = strtotime($end);
		$where[] = ' s.created_at > ' . $end . ' ';
	}
	if(!empty($users)) {
		$uids = implode(',', $users);
		$where[] = ' s.uid IN ( ' . $uids . ' ) ';
	}
	
	// construct the where clause
	if(count($where) > 0) {
		$where_clause = implode(' AND ', $where);
	} else {
		// at least one param is required.
		return FALSE;
	}
	
	$sql = 'SELECT s.*, u.name name 
					FROM status
					LEFT JOIN users ON s.uid = u.id
					WHERE :where
	';
	$query = $pdo->prepare($sql);
	$success = $query->execute(array(':where' => $where));

	if($success) {
		$result = $query->fetchAll();
	} else { // db error
		$result = FALSE;
		print_r($query->errorInfo());
	}
	
	return $result;
}

/**
 * Update a percentage row
 * @param int $sid the id of the row
 * @return bool
 */
function update_mouse_records($sid) {
}

/**
 * Delete a percentage row
 * @param int $sid the id of the row
 * @return bool
 */
function delete_mouse_records($sid) {
}

/**
 * check if the username is valid
 * @param str $name is the user's name
 * @return mixed int if success; bool FALSE on failure
 * @todo: make it work
 */
function is_valid_user($name) {
	$pdo = db_connect();
	$sql = 'SELECT id FROM users WHERE name = :name LIMIT 1';
	$query = $pdo->prepare($sql);
	$success = $query->execute(array(':name' => $name));

	if($success) {
		$result = TRUE;
	} else { // db error
		$result = FALSE;
		print_r($query->errorInfo());
	}
	
	return $result;

}

/**
 * check if the percentage is valid
 * @param int percent should be between 0 and 100 inclusive
 * @return bool
 */
function is_valid_percentage($percent) {
	return (is_numeric($percent) && $percent >= 0 && $percent <= 100);
}


/**
 * check if the time is valid
 * convert the time to a date, format the date, and see if the input matches
 * @param str date formatted 2015-05-07T16:03:05
 * @return bool
 */
function is_valid_time($time) {
	$submitted_time = substr(date('c', strtotime($time)), 0, 19);
	return ($submitted_time === $time);
}

