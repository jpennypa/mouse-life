<?php
/**
 * Mouse Life
 * Functions library
 * @author John Pennypacker <john_pennypacker@brown.edu>
 */

 
/**
 * Checks if the config file exists
 * @return bool
 */
function config_exists() {
	return (file_exists('config.php'));
}

/**
 * Parse a user's log file
 * @param str $log path to a log file to parse
 * @return arr
 */
function parse_log($log) {
	$output = array();
	$data = file_get_contents($log);
	$data = explode("\n", $data);
	foreach($data as $row) {
		$bits = explode(',', $row);
		if(!empty($bits[0]) && !empty($bits[1])) {
			$output[] = array('time' => trim($bits[1]), 'percentage' => trim($bits[0]));
		}
	}
	return $output;
}


/**
 * Connect to the database
 * @return obj; bool FALSE on failure
 */
function db_connect() {
	static $pdo;

	// if $pdo is already set, return the existing connection
	if(!empty($pdo)) {
		return $pdo;
	}
	
	//otherwise, create a new connection
	if(!empty($database_credentials)) {
		die('<p>You must have database credentials in the config.php file to use this application.</p>');
		// @todo: make return meaningful
		return FALSE;
	}

	$pdo = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
	return $pdo;
}
